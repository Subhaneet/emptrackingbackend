const express = require("express");
const bodyParser = require("body-parser");
const session = require("express-session");
let con = require('./dbconnection');
const cors = require("cors");
var bcrypt = require("bcrypt");
const saltRounds = 10;
var passport = require ("passport");
const app = express();
app.use(bodyParser.json());
app.use(cors());
const PORT = 3040;

// Configuring body parser middleware
//app.use(bodyParser.urlencoded({ extended: false }));

app.post('/test', (req, res) => {
res.json({ 'msg': 'sdf' })
console.log("for testing 123");
})

//get all users all list
app.get('/users', (req, res) => {
console.log('user');
con.query('CALL get_all()',
(err, rows) => {
if (err) throw err;

console.log('Data received from employee:');
res.json({ message: "The list of all users are as follows", data: rows[0] });
console.log(rows);
})
});

//get users with specific id only
app.get('/users/:user_id', (req, res) => {
const user_id = req.params.user_id;
//console.log(rows)
con.query(`CALL get_byid(${user_id})`, (err, rows) => {
if (err) throw err;
console.log('Distinct user id received from users:');
console.log(rows);
res.json({ message: "the user information for the given id is", data: rows[0]})

});
});

//post new information about a new user with hashed password

function hashPassword(password){

return new Promise((resolve,reject)=>{
bcrypt.hash(password, saltRounds, (err, hash) => {
console.log('laces',hash)
if(err) return err;
resolve(hash)
})
});
}
app.post('/users', async (req, res) => {
const body = req.body;
// console.log('body',req.body)
// console.log('pwd',body.user_password);

let newpwd = await hashPassword(body.user_password) //await promise
console.log('hash',newpwd)

let insert_query = `CALL insert_newinfo('${body.user_name}','${body.user_email}','${body.user_phone}','${body.user_role}','${newpwd}','${body.user_dob}','${body.user_gender}')`

con.query(insert_query, (err, rows) => {

console.log(rows)
if (err) throw err;
console.log('Post new user information into users:', rows);
console.log(body.insertId)
res.json({ message: `Your information has been sucessfully added and has been assigned the id of ${rows.insertId}`,userId:rows.insertId })
});
});



//put or update new information about a user
function hashPassword(password){

return new Promise((resolve,reject)=>{
bcrypt.hash(password, saltRounds, (err, hash) => {
console.log('laces',hash)
if(err) return err;
resolve(hash)
})
});
}

app.put('/users', async(req, res) => {
const body = req.body
console.log('testing', body);
let newpwd = await hashPassword(body.user_password) //await promise
console.log('hash',newpwd)
con.query(`CALL update_information(${body.user_id},'${body.user_name}','${body.user_email}','${body.user_phone}','${body.user_role}','${newpwd}','${body.user_dob}','${body.user_gender}')`,
(err, rows) => {
if (err) throw err;
console.log('update user id into employee:');
console.log(rows);
res.json({ message: "Your information has been sucessfully updated" })

});
});

//deleting specific id from users
app.delete('/users/:user_id', (req, res) => {
const user_id = req.params.user_id;
con.query(`CALL deleteby_id(${user_id})`, (err, rows) => {
if (err) throw err;
console.log('Delete current employee id from users:');
console.log(rows);
res.json({ message: "deleted user of the given id", data: rows})
});
});

//compare password with that of what is in the database

function decryptPassword(password , hash){
return new Promise((resolve,reject)=>{
bcrypt.compare(password, hash, function(err, res) {
console.log('bcryptin',res)
if(err) return err;
resolve(res)
})
});
}

app.post('/login', async(req, res) => {
const body = req.body
console.log('this', body);
// let newpwd = await decryptPassword(body.user_password)
con.query(`select * from users where user_email=('${body.user_email}')`,
async (err, result) => {
console.log(result[0])
console.log('message', result.user_password)
let check_password = await decryptPassword(body.user_password , result[0].user_password);
console.log('check password==',check_password)
if(check_password){
res.json({ data: result[0], message: "user email correct" })
}else{
res.json({ data: err, message: "login credentials were incorrect" })
}
// if (result[0].length != 0) {
// res.json({ data: result, message: "user email correct" })
// console.log('correct email:');

// }
// else {
// console.log(result);
// res.json({ data: err, message: "login credentials were incorrect" })

// }


});
});


// update users set Active_user = 1 where user_email='${body.user_email}' AND user_password='${body.user_password}'
//showing user authenticity
app.get('/users/approve/:user_id', (req, res) => {
const user_id = req.params.user_id
console.log('fsb' , user_id);
con.query(`call approve_sp(${user_id})`, (err, result) => {
console.log(result)
if (result.affectedRows != 0) {
res.json({ data: result, message: "user active status updated" })
console.log('login sucessfull:');
}
else {
console.log(result);
res.json({ data: err, message: "user doesnot exist" })

}
});


});

app.listen(PORT, () => console.log(`port info ${PORT}!`));
